import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Route, RouterModule } from "@angular/router";

import { MyComponent } from './app.component';
import { MapComponent } from './components/map.components';
import { StartPageComponent } from './components/start-page.component';
import { LoginComponent } from './components/login.component';

import { Http } from "@angular/http";
import { HttpModule } from "@angular/http";

import { PoligonService } from './servises/poligon.service';
import { SensorsDataService } from './servises/sensorsdata.service';
import { UserService } from './servises/user.service';

import { AgmCoreModule } from 'angular2-google-maps/core';

const appRoutes : Route[] = [
  { path: '', component: StartPageComponent  },
  { path: 'map', component: MapComponent },
  { path: 'home', component: StartPageComponent },
  { path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [
    HttpModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDL3hiX6PfKTO4I25z_X92fM3U2OaM02yg'
    })
  ],
  providers: [ PoligonService,  SensorsDataService, UserService],
  declarations: [ MyComponent, MapComponent, StartPageComponent, LoginComponent ],
  bootstrap: [ MyComponent ]
})
export class AppModule {}