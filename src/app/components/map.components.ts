import { Component, OnInit } from '@angular/core';
import { SebmGoogleMap, SebmGoogleMapPolygon, LatLngLiteral } from 'angular2-google-maps/core';
import { Poligon } from "../models/poligon.model";
import { PoligonService } from "../servises/poligon.service";
import { SensorsDataService } from "../servises/sensorsdata.service";

@Component({
//  selector: 'map',
 styleUrls: [`../views/map.component.css`],
 templateUrl: `../views/map.component.html`,
 providers: [PoligonService, SensorsDataService]
})

export class MapComponent implements OnInit {

    isInfoVisible: boolean;
    CO2: number;
    NH3: number;
    smoke: number;
    sound: number;

    isFAQVisible: boolean;
    lat: number = 50.005734;
    lng: number = 36.229169;
    zoom: number = 10;

    searchText: string;
    poligons: Array<Poligon>=[];
    paths: Array<Array<LatLngLiteral>>=[];
    public constructor(private poligonsService: PoligonService,
                      private sensorsDataServise: SensorsDataService) { }

    ngOnInit() {  
        this.isInfoVisible=false;
        this.isFAQVisible=false;

        this.searchText="";
        this.poligonsService.DrawPoligons().then(model => {
        console.log(model);
        if(model!=null) {
            this.poligons=model;
            console.log("map component got poligons");
            this.poligons.forEach(e=>this.paths.push(e.paths));
            console.log(this.poligons);
            console.log(this.paths);
        }
    });
}

click(p: Poligon){
 this.sensorsDataServise.GetSensorsData( p.id).then(model => {
        console.log("click on id " + p.id);
        if(model!=null) {
            console.log("map component got sensorsData");
            console.log(model);
            this.CO2=model.CO2;
            this.NH3=model.NH3;
            this.smoke=model.smoke;
            this.sound=model.sound;
        }
        this.isInfoVisible=true;
    });
}

FAQClose(){
   this.isFAQVisible=false;
   this.isInfoVisible=false;
}

FAQOpen(){
    this.isFAQVisible=true;
}

onSearch(){
    window.location.assign("https://www.google.com/search?q="+this.searchText);
}
    
}

