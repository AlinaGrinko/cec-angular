import { Component} from '@angular/core';
import { MapComponent } from "../components/map.components";

import{PoligonService} from "../servises/poligon.service";
import { Route, RouterModule, Router } from "@angular/router";
//import { SebmGoogleMap, SebmGoogleMapPolygon, LatLngLiteral } from 'angular2-google-maps/core';

@Component({
selector: 'start-page',
 styleUrls: [`../css/start-page.component.css`],
 templateUrl: `../views/start-page.component.html`,
 providers: [PoligonService]
})
export class StartPageComponent {
    constructor (private router : Router) {}

    onStart(){
        // this.router.navigate(['map']);
        this.router.navigate(['login']);
    }
}