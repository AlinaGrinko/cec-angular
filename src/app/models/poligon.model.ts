import {LatLngLiteral } from 'angular2-google-maps/core';


export class Poligon {
    public id: number;
    public center: LatLngLiteral;

    public paths: LatLngLiteral[]=[
        {lat:0, lng:10},
        {lat:0, lng:20},
        {lat:10, lng:20},
        {lat:10, lng:10},
        {lat:0, lng:10}
    ];
    
    constructor(id:number=null, center:LatLngLiteral=null,p:LatLngLiteral[]=null) {
        this.id=id;
        this.center=center;
        this.paths=p;
    }
}