export class User {
    public id: number;
    public login: string;
    public password: string;
    public role: string;
    
    constructor(id:number=null, login:string=null, password:string=null, role: string=null) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public isAdmin = function() {
        return this.role == "ADMIN";
    }
}