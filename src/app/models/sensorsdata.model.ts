export class SensorsData {
    public id: number;
    public CO2: number;
    public NH3: number;
    public smoke: number;
    public sound: number;
    
    constructor(id:number=null, CO2:number=null, NH3:number=null, 
                         smoke:number=null, sound: number=null) {
        this.id=id;
        this.CO2=CO2;
        this.NH3=NH3;
        this.smoke=smoke;
        this.sound=sound;
    }
}