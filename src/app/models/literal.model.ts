import {LatLngLiteral } from 'angular2-google-maps/core';
export class Literal implements LatLngLiteral{
    public lat: number;
    public lng: number;

    public constructor(lat:number, lng:number){
        this.lat=lat;
        this.lng=lng;
    }
}