import { Component} from '@angular/core';
import { MapComponent } from "./components/map.components";

import{ PoligonService } from "./servises/poligon.service";
import { SensorsDataService } from './servises/sensorsdata.service';

import { Route, RouterModule, Router } from "@angular/router";
//import { SebmGoogleMap, SebmGoogleMapPolygon, LatLngLiteral } from 'angular2-google-maps/core';

@Component({
selector: 'app-root',
 styleUrls: [`./app.component.css`],
 templateUrl: `./app.component.html`,
 providers: [PoligonService, SensorsDataService]
})
export class MyComponent {
    constructor (private router : Router) {
    //  this.router.navigate(['home']);
    }
    // onStart(){
    //      this.router.navigate(['map']);
    // }
}
