import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions  } from "@angular/http";
import{ User } from "../models/user.model";

import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {

    private path:string = 'http://localhost:8080/CCC_SL/GetUser?login=';

    public constructor(private http: Http) { }

 public GetUsersData(login:string, password:string) : Promise<User> {

     this.path += login + '&password=' + password; 
                return this.http.get(this.path).toPromise()
        .then(rs => {
            var res = rs.json();
            console.log(res);
            if (res != null) {
               console.log(res+"!=0");
               var user = this.transformToUser(res);
               return user;
            }
            else {
                return null;
            }   
        });
    }

    private transformToUser(res: any): User {
        if (res != null) {
            console.log("in transformToUser");
            console.log(res); 
            var result = JSON.parse(JSON.stringify(res));
            console.log(result + "after parsing"); 
            return new User(result.id, result.login, result.password, result.role);
        }
        else{
            return null;
        }
    }
}