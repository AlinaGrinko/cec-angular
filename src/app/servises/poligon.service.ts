import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions  } from "@angular/http";

import{ Poligon } from "../models/poligon.model";
import{ Literal } from "../models/literal.model";
import {LatLngLiteral, LatLng } from 'angular2-google-maps/core';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class PoligonService {

    private path:string = 'http://localhost:8080/CCC_SL/GetPoligons';

    public constructor(private http: Http) { }

 public DrawPoligons() : Promise<Poligon[]> {
                return this.http.get(this.path).toPromise()
        .then(rs => {
            var res = rs.json();
            console.log(res);
            if (res != null) {
               console.log(res+"!=0");
               var poligons = this.transformToPoligonArray(res);
               return poligons;
            }
            else {
                return null;
            }   
        });
    }

    private transformToPoligon(res: any): Poligon {
        if (res != null) {
            console.log("in transformToPoligon");
            console.log(res); 
            var result = JSON.parse(JSON.stringify(res));
            console.log(result+"after parsing"); 
            return new Poligon(res.id,this.transformToLatLngLiteral(result.center),
                                      this.transformToLatLngArray(result.paths));
        }
        else{
            return null;
        }
    }

    private transformToPoligonArray(res: any): Poligon[] {
        console.log("in transformToPoligonArra");
        return res.map(myobject => this.transformToPoligon(myobject));
    }

    private transformToLatLngArray(res: any): LatLngLiteral[] {
        console.log("in transformToLatLngArra");
        return res.map(myobject => this.transformToLatLngLiteral(myobject));
    }

    private transformToLatLngLiteral(res: any): LatLngLiteral {
        if (res != null) {
            console.log("in transformToLatLngLiteral");
            console.log(res); 
            res = JSON.parse(JSON.stringify(res));
            console.log(res); 
            return  new Literal(res.lat,res.lng);
        }
        else{
            return null;
        }
    }

}