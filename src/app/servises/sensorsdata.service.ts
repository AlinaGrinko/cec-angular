import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions  } from "@angular/http";

import{ SensorsData } from "../models/sensorsdata.model";
import 'rxjs/add/operator/toPromise';

@Injectable()
export class SensorsDataService {

    private path:string = 'http://localhost:8080/CCC_SL/GetSensorsData';

    public constructor(private http: Http) { }

 public GetSensorsData(number:number) : Promise<SensorsData> {
                return this.http.get(this.path+"?number="+ number).toPromise()
        .then(rs => {
            var res = rs.json();
            console.log(res);
            if (res != null) {
               console.log(res+"!=0");
               var sensorsData = this.transformToSensorsData(res);
               return sensorsData;
            }
            else {
                return null;
            }   
        });
    }

    private transformToSensorsData(res: any): SensorsData {
        if (res != null) {
            console.log("in transformToSensorsData");
            console.log(res); 
            var result = JSON.parse(JSON.stringify(res));
            console.log(result+"after parsing"); 
            return new SensorsData(res.id,res.CO2,res.NH3,res.smoke,res.sound);
        }
        else{
            return null;
        }
    }

    // private transformToPoligonArray(res: any): Poligon[] {
    //     console.log("in transformToPoligonArra");
    //     return res.map(myobject => this.transformToPoligon(myobject));
    // }

    // private transformToLatLngArray(res: any): LatLngLiteral[] {
    //     console.log("in transformToLatLngArra");
    //     return res.map(myobject => this.transformToLatLngLiteral(myobject));
    // }

    // private transformToLatLngLiteral(res: any): LatLngLiteral {
    //     if (res != null) {
    //         console.log("in transformToLatLngLiteral");
    //         console.log(res); 
    //         res = JSON.parse(JSON.stringify(res));
    //         console.log(res); 
    //         return  new Literal(res.lat,res.lng);
    //     }
    //     else{
    //         return null;
    //     }
    // }

}