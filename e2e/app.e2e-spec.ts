import { CecAngularPage } from './app.po';

describe('cec-angular App', function() {
  let page: CecAngularPage;

  beforeEach(() => {
    page = new CecAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
